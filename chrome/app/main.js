(function () {
	var extensionId 	= "mlglngjgefkbflbmelghfeijmojocnbi";
	var isLaunched 		= false;
	var storage  		= chrome.storage.local;
	var storageSet 		= storage.set.bind(storage);
	var storageGet 		= storage.get.bind(storage);
	var storageClear	= storage.clear.bind(storage);
	var fileSystem 		= chrome.fileSystem;

	function createWindow(path, callback) {
		var id = path.replace(/\/|\./g, "_");
		var screenWidth = screen.availWidth;
		var screenHeight = screen.availHeight;
		var width = 200;
		var height = 200;
		var minHeight = 200;
		var maxHeight = 200;
		chrome.app.window.create(path, {
			id: id,
			minWidth  : width,
			minHeight : minHeight,
			maxWidth  : width,
			maxHeight : minHeight,
			bounds: {
				width: width,
				height: height,
				left: Math.round((screenWidth-width)/2),
				top: Math.round((screenHeight-height)/2)
			},
			frame: "chrome"
		}, function(win) {
			callback && callback(win);
		});
	}

	var FileChecker = (function () {
		var fileEntries = {};
		var defaultFolder = null;
		var defaultFolderEntry = null;
		var defaultWritebleFolderEntry = null;

		function MessageToPlugin () {
			this.getTask = function () {};
			this.eventName = function () {};
			this.doIt = function (pathName, content, isDefault) {
				chrome.runtime.sendMessage(extensionId, {
					name: this.eventName(),
					task: this.getTask(),
					info: pathName,
					content: content,
					isDefault: isDefault
				});
			};
		}
		function ContentChanged (taskName) {
			this.eventName = function () {
				return "app:fileContent";
			};
			this.getTask = function () {
				return taskName;
			};
		}
		ContentChanged.prototype = new MessageToPlugin();
		ContentChanged.prototype.constructor = ContentChanged;
		function FileChosen (taskName) {
			this.eventName = function () {
				return "app:fileChosen";
			};
			this.getTask = function () {
				return taskName;
			};
		}
		FileChosen.prototype = new MessageToPlugin();
		FileChosen.prototype.constructor = FileChosen;
		function FileChecked (taskName) {
			this.eventName = function () {
				return "app:fileChecked";
			};
			this.getTask = function () {
				return taskName;
			};
		}
		FileChecked.prototype = new MessageToPlugin();
		FileChecked.prototype.constructor = FileChecked;
		function FileContentSet (taskName) {
			this.eventName = function () {
				return "app:fileContentSet";
			};
			this.getTask = function () {
				return taskName;
			};
		}
		FileContentSet.prototype = new MessageToPlugin();
		FileContentSet.prototype.constructor = FileContentSet;		
		function FileUnbound (taskName) {
			this.eventName = function () {
				return "app:fileUnbound";
			};
			this.getTask = function () {
				return taskName;
			};
		}
		FileUnbound.prototype = new MessageToPlugin();
		FileUnbound.prototype.constructor = FileUnbound;

		function TasksInfo () {
			this.eventName = function () {
				return "app:tasksInfo";
			};
			this.getTask = function () {
				return "";
			};
			this.doIt = function (tasks) {
				chrome.runtime.sendMessage(extensionId, {
					name: this.eventName(),
					tasks: tasks
				});
			};
		}
		TasksInfo.prototype = new MessageToPlugin();
		TasksInfo.prototype.constructor = TasksInfo;

		function DefaultFolder () {
			this.eventName = function () {
				return "app:defaultFolder";
			};
			this.getTask = function () {
				return "";
			};
			this.doIt = function () {
				var folderInfo = defaultFolder || {name: "", displayPath: ""};
				chrome.runtime.sendMessage(extensionId, {
					name: this.eventName(),
					info: folderInfo.name,
					path: folderInfo.displayPath
				});
			};
		}
		DefaultFolder.prototype = new MessageToPlugin();
		DefaultFolder.prototype.constructor = DefaultFolder;

		function fileLoadedFail (taskName) {
			(new FileChosen(taskName)).doIt("", "");
		}
		
		function fileLoadedOK (evt, taskName) {
			var fileContent = evt.target.result;
			var fileInfo = fileEntries[taskName];
			if (fileInfo) {
				var filePath = fileEntries[taskName].entry.name;
				if (!fileInfo.content && "string" !== typeof(fileInfo.content)) {
					fileInfo.content = fileContent;
					(new FileChosen(taskName)).doIt(filePath, fileContent);
				} else {
					var md5FileContent = CryptoJS.MD5(fileContent).toString();
					var md5CachedFileContent = CryptoJS.MD5(fileInfo.content).toString();
					if (md5FileContent !== md5CachedFileContent) {
						fileEntries[taskName].content = fileContent;
						(new ContentChanged(taskName)).doIt(filePath, fileContent);
					}
				}
			}
		}
		function checkFile (taskName, callbackOk, callbackFail) {
			if (fileEntries[taskName]) {
				var fileEntry = fileEntries[taskName].entry;
				if (fileEntry) {
					fileEntry.file(function(file) {
						var reader = new FileReader();
						reader.onerror = function (evt) {
							var data = {};
							data[taskName] = {};
							storageSet(data, function () {
								(callbackFail || fileLoadedFail)(taskName);
							});
						};
						reader.onloadend = callbackOk || function (evt) {
							fileLoadedOK(evt, taskName);
						};
						reader.readAsText(file);
						delete reader;
					}, function (evt) {
						callbackFail && callbackFail(evt);
					});
				} else {
					var data = {};
					data[taskName] = {};
					storageSet(data, function () {
						(callbackFail || fileLoadedFail)(taskName);
					});
				}
			}
		}
		function restoreEntry (taskName, callback) {
			storageGet(taskName, function (data) {
				if (data && data[taskName]) {
					data = data[taskName];
					if (data["cached_file"]) {
						if (!fileEntries[taskName]) {
							fileEntries[taskName] = {};
						}
						fileSystem.restoreEntry(data["cached_file"], (callback || function (entry) {
							fileEntries[taskName].entry = entry
							checkFile(taskName);
						}));
					}
				}
			});
		}
		function clearTaskInfo(taskName) {
			if (fileEntries[taskName]) {
				fileEntries[taskName].content = null;
				fileEntries[taskName].entry = null;
				fileEntries[taskName] = null;
			}
		}
		function bind (taskName) {
			clearTaskInfo(taskName);
			restoreEntry(taskName);	
		}
		function bindCreated(taskName, content) {
			clearTaskInfo(taskName);
			restoreEntry (taskName, function (entry) {
				fileEntries[taskName].entry = entry;
				writeToFile (taskName, content, function (isOk) {
					if (true === isOk) {
						(new FileChosen(taskName)).doIt(entry.name, content);
					}
				});
			});
		}
		function checkTrue (data, taskName, ext) {
			fileSystem.restoreEntry(data[taskName]["cached_file"], function (entry) {
				fileEntries[taskName].entry = entry
				checkFile(taskName, function (evt) {
					var fileContent = evt.target.result;
					var filePath = fileEntries[taskName].entry.name;
					(new FileChecked(taskName)).doIt(filePath, fileContent, data[taskName].isDefault)
				}, function (evt) {
					if (data[taskName].isDefault) {
						data[taskName] = {};
						storageSet(data, function () {
							check(taskName, ext);
						});						
					} else {
						(new FileChecked(taskName)).doIt("", "");	
					}
				})
			});
		}
		function workOurFile (taskName, cachedFile, isCreated, displayPath, ext) {
			var _data = {};
			_data[taskName] = {
				cached_file: cachedFile,
				isDefault: true,
				displayPath: displayPath
			};
			storageSet(_data, function () {
				if (!isCreated) {
					checkTrue(_data, taskName, ext);
				} else {
					bindCreated(taskName, content)
				}
			});
		}
		function checkFileInDefaultFolder(taskName, ext, callback) {
			isFileExist(taskName, ext, function (exists, cached_file) {
				if (exists) {
					workOurFile(taskName, cached_file, null, null, ext);
				} else {
					createFileInDefaultFolder(taskName, ext, function (OK, cached_file, displayPath, entry) {
						if (callback) {
							callback(cached_file, displayPath, entry);
						} else {
							if (OK) {
								workOurFile(taskName, cached_file, null, displayPath, ext);
							} else {
								(new FileChecked(taskName)).doIt("", "");
							}	
						}
					});
				}
			});
		}
		function check(taskName, ext) {
			storageGet(taskName, function (data) {
				fileEntries[taskName] = {};
				if (data && data[taskName] && data[taskName]["cached_file"]) {
					// If we already found file
					checkTrue(data, taskName, ext);
				} else if (defaultFolder) {
					// if default folder is set
					checkFileInDefaultFolder(taskName, ext);
				} else {
					(new FileChecked(taskName)).doIt("", "");
				}
			});
		}
		function getFile (taskName) {
			restoreEntry(taskName);
		}
		function unbindFile (taskName) {
			clearTaskInfo(taskName);
			var data = {};
			data[taskName] = {};
			storageSet(data, function () { (new FileUnbound(taskName)).doIt(); });
		}
		function writeToFile (taskName, content, callback) {
			if (fileEntries[taskName]) {
				var entry = fileEntries[taskName].entry;
				var md5FileContent = CryptoJS.MD5(content).toString();
				var md5CachedFileContent = CryptoJS.MD5(fileEntries[taskName].content).toString();
				if (md5FileContent !== md5CachedFileContent) {
					entry.createWriter(function (writer)  {
						writer.truncate(0);
						writer.onerror = function(err) {
							callback && callback(false, "FileSystem writeFile failure");
						};
						writer.onwriteend = function () {
							var _blob = new Blob([content], {type: 'text/plain'});
							writer.write(_blob);
							writer.onerror = function(err) {
								callback && callback(false, "FileSystem writeFile failure");
							};
							writer.onwriteend = function () {
								fileEntries[taskName].content = content;
								callback && callback(true);
							};
							delete _blob;
						};
					}, function () {
						callback && callback(false, "FileSystem createWriter failure");
					});
				}
			} else {
				callback && callback(false, "FileSystem getFile failure");
			}
		}

		function writeToFileDef(taskName, ext, content) {
			writeToFile (taskName, content, function (isOk) {
				if (true === isOk) {
					(new FileContentSet(taskName)).doIt(taskName + '.' + ext, content);
				}
			});
		}

		function isFileExist (filename, ext, callback) {
			var _filename = filename.split("_").join(".");
			if (defaultWritebleFolderEntry) {
				fileSystem.getWritableEntry(defaultWritebleFolderEntry, function (entry) {
					entry.getFile(_filename, {create : false}, function (entry) {
						callback(true, fileSystem.retainEntry(entry));
					}, function (evt) {
						callback(false);
					});
				});
			} else {
				callback(false, {err: 'pls choose default folder'})
			}
		}

		function createFileInDefaultFolder(taskName, ext, callback) {
			var filename = taskName.split("_").join(".");
			fileSystem.getWritableEntry(defaultWritebleFolderEntry, function(entry) {
				entry.getFile(filename, {create : true}, function(entry) {
					fileSystem.getDisplayPath(entry, function (displayPath) {
						if (entry) {
							callback(true, fileSystem.retainEntry(entry), displayPath, entry);
						} else {
							callback(false);
						}
					});
				}, function (evt) {
					callback(false);
				});
			});
		}

		function setDefaultFolder (data, taskName, content) {
			defaultFolder = data.defaultFolder;
			// Get entry object
			if (defaultFolder) {
				fileSystem.restoreEntry(defaultFolder['cached_file'], function (entry) {
					defaultFolderEntry = entry;
					// Get writable entry
					fileSystem.getWritableEntry(defaultFolderEntry, function (entry) {
						defaultWritebleFolderEntry = entry || null;
						if (defaultWritebleFolderEntry) {
							if  (taskName) {
								fileEntries[taskName] = {};
								var ext = taskName.split("_")[1];
								console.log(taskName, ext);
								checkFileInDefaultFolder(taskName, ext, function (cached_file, displayPath, entry) {
									var _data = {};
									_data[taskName] = {cached_file: cached_file, isDefault: true, displayPath: displayPath};
									storageSet(_data, function () {
										fileEntries[taskName].entry = entry;
										writeToFile(taskName, content, function (isOk) {
											if (true === isOk) {
												(new FileChosen(taskName)).doIt(entry.name, content);
											}
										});
									});
								});
							}
						} else {
							unbindDefaultFolder();
						}
					});
				});
			}
		}

		function init () {
			storageGet("defaultFolder", function (dataFolder) {
				setDefaultFolder(dataFolder);
			});
		}

		function unbindDefaultFolder (taskName) {
			storageClear(function () {
				defaultFolder = defaultFolderEntry = null;
				defaultWritebleFolderEntry = null;
				for (var _taskName in fileEntries) {
					clearTaskInfo(_taskName);
				}
				(new FileUnbound(taskName)).doIt('', '', false);
			});
		}

		function getTasks () {
			storageGet(function (tasks) {
				(new TasksInfo()).doIt(tasks);
			});
		}

		return {
			init: init,
			bind: bind,
			check: check,
			getFile: getFile,
			getTasks: getTasks,
			writeToFile: writeToFile,
			writeToFileDef: writeToFileDef,
			bindCreated: bindCreated,
			unbindFile: unbindFile,
			setDefaultFolder: setDefaultFolder,
			unbindDefaultFolder: unbindDefaultFolder
		}
	})();

	function MessageToPopup () {
		this.eventName = function () {};
		this.doIt = function (win, data) {
			var _window = win.contentWindow;
			var _document = _window.document;
			var event = _document.createEvent('CustomEvent');
			event.initCustomEvent(this.eventName(), true, true, {
				data: data
			});
			_window.dispatchEvent(event);
		};
	}

	function SetTaskToPopup () {
		this.eventName = function () {
			return "app:setTaskName";
		};
	}
	SetTaskToPopup.prototype = new MessageToPopup();
	SetTaskToPopup.prototype.constructor = SetTaskToPopup;

	function Message () {
		this.doIt = function () {};
	}
	function BindFolderMessage (taskName) {
		function chosen (evt, win, content) {
			var detail = evt.detail;
			var isOk = detail.isOk;
			var data = detail.data;
			if (true == isOk) {
				FileChecker.setDefaultFolder(data, taskName, content);
				win.close();
			}
		}
		this.doIt = function (content) {
			createWindow('saver/saver.html', function (win) {
				var _window = win.contentWindow;
				_window.addEventListener("popup:chosenDefaultFolder", function (evt) {
					chosen(evt, win, content);
				});
			});
		};
	}
	BindFolderMessage.prototype = new Message();
	BindFolderMessage.prototype.constructor = BindFolderMessage;

	function NewWebContentMessage (taskName) {
		this.doIt = function (content) {
			FileChecker.writeToFile(taskName, content);
		};
	}

	function NewWebContentDefaultMessage(taskName, ext) {
		this.doIt = function (content) {
			FileChecker.writeToFileDef(taskName, ext, content);
		};	
	}

	function GetFileMessage (taskName) {
		this.doIt = function () {
			FileChecker.getFile(taskName);
		};
	}
	GetFileMessage.prototype = new Message();
	GetFileMessage.prototype.constructor = GetFileMessage;

	function UnbindFile (taskName) {
		this.doIt = function () {
			FileChecker.unbindFile(taskName);
		};
	}
	UnbindFile.prototype = new Message();
	UnbindFile.prototype.constructor = UnbindFile;

	function CheckFile (taskName, ext) {
		this.doIt = function () {
			FileChecker.check(taskName, ext);
		};
	}

	function MessageManager () {
		var m_callbacks = {};
		this.fireEvent = function (eventName, message, sender, sendResponse) {
			var callbacks = m_callbacks[eventName];
			if (callbacks && callbacks.length) {
				for (var i = 0 , li = callbacks.length ; i < li ; ++i) {
					callbacks[i](eventName, message, sender, sendResponse);
				}
			}
		};
		this.addEventListener = function (eventName, callback) {
			if (!m_callbacks[eventName]) {
				m_callbacks[eventName] = [callback];
			} else {
				m_callbacks[eventName].push(callback)
			}
		};
	}

	function PluginMessageManager () {
		var self = this;
		chrome.runtime.onMessageExternal.addListener(function (message, sender, sendResponse) {
			self.fireEvent(message.name, message, sender, sendResponse);
		});
	}
	PluginMessageManager.prototype = new MessageManager();
	PluginMessageManager.prototype.constructor = PluginMessageManager;

	var g_pluginMessageManager = new PluginMessageManager();
	g_pluginMessageManager.addEventListener("plugin:bindFolder", function (evt, message) {
		(new BindFolderMessage(message.task)).doIt(message.content);
	});
	g_pluginMessageManager.addEventListener("plugin:unbindFolder", function (evt, message) {
		FileChecker.unbindDefaultFolder(message.task);
	});
	g_pluginMessageManager.addEventListener("plugin:getFile", function (evt, message) {
		(new GetFileMessage(message.task)).doIt();
	});
	g_pluginMessageManager.addEventListener("plugin:webContent", function (evt, message) {
		(new NewWebContentMessage(message.task)).doIt(message.content);
	});
	g_pluginMessageManager.addEventListener("plugin:webContentDefault", function (evt, message) {
		(new NewWebContentDefaultMessage(message.task, message.ext)).doIt(message.content);
	});
	g_pluginMessageManager.addEventListener("plugin:unbindFile", function (evt, message) {
		(new UnbindFile(message.task)).doIt(message.content);
	});
	g_pluginMessageManager.addEventListener("plugin:checkFile", function (evt, message) {
		(new CheckFile(message.task, message.ext)).doIt();
	});
	g_pluginMessageManager.addEventListener("options:getTasks", function (evt, message) {
		FileChecker.getTasks();
	});

	FileChecker.init();
	
	chrome.app.runtime.onLaunched.addListener(function(data) {
		window.open("https://checkio.org/");
	});
})();