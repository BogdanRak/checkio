(function (_window, _document) {
	var storage  	= chrome.storage.local;
	var storageSet 	= storage.set.bind(storage);
	var fileSystem 	= chrome.fileSystem;
	var g_taskName = "";

	function MessageToApp () {
		this.eventName = function () {};
		this.doIt = function (isOk, data) {
			var _document = _window.document;
			var event = _document.createEvent('CustomEvent');
			event.initCustomEvent(this.eventName(), true, true, {
				isOk: isOk,
				data: data
			});
			_window.dispatchEvent(event);
		};
	}

	function FolderChosen () {
		this.eventName = function () {
			return "popup:chosenDefaultFolder";
		};
	}
	FolderChosen.prototype = new MessageToApp();
	FolderChosen.prototype.constructor = FolderChosen;

	function handleFolder (entry) {
		if (entry) {
			var data = {};
			chrome.fileSystem.getDisplayPath(entry, function (displayPath) {
				data["defaultFolder"] = {
					cached_file: fileSystem.retainEntry(entry),
					displayPath: displayPath,
					name: entry.name
				}
				storageSet(data, function () {
					(new FolderChosen()).doIt(true, data);
				});
			});
		} else {
			(new FolderChosen()).doIt(false, {}, chrome.runtime.lastError);
		}
	}
	function openFolder () {
		fileSystem.chooseEntry({type : 'openDirectory'}, function (entry) {
			handleFolder(entry);
		});
	}
	function addEventListener(id, eventName, handler) {
		var element = _document.getElementById(id);
		if (element) {
			element.addEventListener(eventName, handler);
		}
	}
	_document.addEventListener("DOMContentLoaded", function(evt) {
		addEventListener("choose", "click", openFolder);
	}, false);

	_window.addEventListener("app:setTaskName", function (evt) {
		g_taskName = evt.detail.data;
	});
})(window, document);