rm -rf build_app.zip #
#
mkdir ./build_app #
cp ../app/manifest.json ./build_app/manifest.json #
cp ../app/crypto.js     ./build_app/crypto.js #
cp ../app/icon16.png    ./build_app/icon16.png #
cp ../app/icon128.png   ./build_app/icon128.png #
java -jar GoogleCompiler.jar --js=../app/main.js --js_output_file=./build_app/main.js #
#
mkdir build_app/saver #
cp ../app/saver/saver.css     ./build_app/saver/saver.css #
cp ../app/saver/saver.html    ./build_app/saver/saver.html #
java -jar GoogleCompiler.jar --js=../app/saver/saver.js --js_output_file=./build_app/saver/saver.js #
#
#
#
zip -r build_app.zip ./build_app #
#
#
rm -rf build_plugin.zip #
mkdir ./build_plugin #
#
cp ../extension/manifest.json  ./build_plugin/manifest.json #
cp ../extension/icon16.png     ./build_plugin/icon16.png #
cp ../extension/icon128.png    ./build_plugin/icon128.png #
#
java -jar GoogleCompiler.jar --js=../extension/content.js --js_output_file=./build_plugin/content.js #
java -jar GoogleCompiler.jar --js=../extension/main.js --js_output_file=./build_plugin/main.js #
java -jar GoogleCompiler.jar --js=../extension/versionlog.js --js_output_file=./build_plugin/versionlog.js #
#
zip -r build_plugin.zip ./build_plugin #
#
rm -rf ./build_app #
rm -rf ./build_plugin #
#