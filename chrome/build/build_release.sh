rm -rf build_app.zip
mkdir ./build_app #
cat ../app/manifest.json | sed 's/"key".*",//g' > ./build_app/manifest.json #
cp ../app/saver.css     ./build_app/saver.css #
cp ../app/saver.html    ./build_app/saver.html #
cp ../app/crypto.js     ./build_app/crypto.js #
cp ../app/icon16.png    ./build_app/icon16.png #
cp ../app/icon128.png   ./build_app/icon128.png #
cp ../pem_app/key.pem   ./build_app/key.pem #
#
java -jar GoogleCompiler.jar --js=../app/saver.js --js_output_file=./build_app/saver.js #
java -jar GoogleCompiler.jar --js=../app/main.js --js_output_file=./build_app/main.js #
#
zip -r build_app.zip ./build_app #
#
#
rm -rf build_plugin.zip #
mkdir ./build_plugin #
#
cat ../extension/manifest.json | sed 's/"key".*",//g' > ./build_plugin/manifest.json #
cp ../extension/icon16.png     ./build_plugin/icon16.png #
cp ../extension/icon128.png    ./build_plugin/icon128.png #
cp ../pem_ext/key.pem          ./build_plugin/key.pem #
#
#
java -jar GoogleCompiler.jar --js=../extension/content.js --js_output_file=./build_plugin/content.js #
java -jar GoogleCompiler.jar --js=../extension/main.js --js_output_file=./build_plugin/main.js #
#
zip -r build_plugin.zip ./build_plugin #
#
rm -rf ./build_app #
rm -rf ./build_plugin #
#