#!/usr/bin/python

import os, zipfile


def zipDir(source_dir, output_file):
	source_dir = os.path.abspath(source_dir)

	# Build file list
	filelist = []
	for dirpath, dirs, files in os.walk(source_dir):
		for file in files:
			abspath = os.path.join(dirpath, file)
			relpath = os.path.relpath(abspath, source_dir).replace('\\', '/')
			handle = open(abspath, 'rb')
			filelist.append((abspath, relpath, handle.read()))
			handle.close()

	# Write everything into a ZIP file, with zigbert.rsa as first file
	zip = zipfile.ZipFile(output_file, 'w', zipfile.ZIP_DEFLATED)
	for filepath, relpath, data in filelist:
		zip.writestr(relpath, data)

if __name__ == "__main__":
	zipDir('../ext', 'checkio_ext.xpi')
