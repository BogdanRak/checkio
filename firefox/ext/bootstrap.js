var g_extension = (function () {

	var EMULATE_APP_SHUTDOWN = -1;
	var APP_SHUTDOWN = 2;
	var ADDON_UNINSTALL = 6;
	var CONSTANTS = {
		"-1" : "EMULATE_APP_SHUTDOWN",
		"1" : "APP_STARTUP",
		"2" : "APP_SHUTDOWN",
		"3" : "ADDON_ENABLE",
		"4" : "ADDON_DISABLE",
		"5" : "ADDON_INSTALL",
		"6" : "ADDON_UNINSTALL",
		"7" : "ADDON_UPGRADE",
		"8" : "ADDON_DOWNGRADE"
	};

	var BOUND_FILE = {
		'file': 1,
		'defaultFolder': 3
	};

	var Cmps = Components;
	var Cc = Cmps.classes;
	var Ci = Cmps.interfaces;
	var Cu = Cmps.utils;
	Cu["import"]("resource://gre/modules/Services.jsm");
	var Ss = Services;
	var mediator = Ss.wm;
	var watcher = Ss.ww;

	var timers = (function () {
		var objects = {};
		var count = 0;
		var ONCE = Ci.nsITimer.TYPE_ONE_SHOT;
		var REPEAT = Ci.nsITimer.TYPE_REPEATING_SLACK;
			
		function createTimer(type, callback, delay, args) {
			var id = count++;
			var timer = objects[id] = Cc['@mozilla.org/timer;1'].createInstance(Ci.nsITimer);
			timer.initWithCallback({
				notify: function () {
					if (type === ONCE) {
						delete objects[id];
					}
					try {
						callback(null, args);
					} catch(e) { }
				}
			}, delay || 0, type);
			return id;
		}
		
		function destroyTimer(id) {
			var timer = objects[id];
			if (timer) {
				timer.cancel();
				delete objects[id];
			}
		}
		
		return {
			reset : function () {
				for (var key in objects) {
					destroyTimer(key);
				}
			},
			setTimeout : function (callback, delay) {
				return createTimer(ONCE, callback, delay, Array.slice(arguments, 2));
			},
			clearTimeout : function (id) {
				destroyTimer(id);
			}
		};
	})();

	var console = (function () {
		return {
			log : function (data) {
				Services.console.logStringMessage(data);
				console.log(data);
			}
		};
	})();

	var CryptoJS = (function () {
		function md5cycle(x, k) {
			var a = x[0], b = x[1], c = x[2], d = x[3];

			a = ff(a, b, c, d, k[0], 7, -680876936);
			d = ff(d, a, b, c, k[1], 12, -389564586);
			c = ff(c, d, a, b, k[2], 17,  606105819);
			b = ff(b, c, d, a, k[3], 22, -1044525330);
			a = ff(a, b, c, d, k[4], 7, -176418897);
			d = ff(d, a, b, c, k[5], 12,  1200080426);
			c = ff(c, d, a, b, k[6], 17, -1473231341);
			b = ff(b, c, d, a, k[7], 22, -45705983);
			a = ff(a, b, c, d, k[8], 7,  1770035416);
			d = ff(d, a, b, c, k[9], 12, -1958414417);
			c = ff(c, d, a, b, k[10], 17, -42063);
			b = ff(b, c, d, a, k[11], 22, -1990404162);
			a = ff(a, b, c, d, k[12], 7,  1804603682);
			d = ff(d, a, b, c, k[13], 12, -40341101);
			c = ff(c, d, a, b, k[14], 17, -1502002290);
			b = ff(b, c, d, a, k[15], 22,  1236535329);

			a = gg(a, b, c, d, k[1], 5, -165796510);
			d = gg(d, a, b, c, k[6], 9, -1069501632);
			c = gg(c, d, a, b, k[11], 14,  643717713);
			b = gg(b, c, d, a, k[0], 20, -373897302);
			a = gg(a, b, c, d, k[5], 5, -701558691);
			d = gg(d, a, b, c, k[10], 9,  38016083);
			c = gg(c, d, a, b, k[15], 14, -660478335);
			b = gg(b, c, d, a, k[4], 20, -405537848);
			a = gg(a, b, c, d, k[9], 5,  568446438);
			d = gg(d, a, b, c, k[14], 9, -1019803690);
			c = gg(c, d, a, b, k[3], 14, -187363961);
			b = gg(b, c, d, a, k[8], 20,  1163531501);
			a = gg(a, b, c, d, k[13], 5, -1444681467);
			d = gg(d, a, b, c, k[2], 9, -51403784);
			c = gg(c, d, a, b, k[7], 14,  1735328473);
			b = gg(b, c, d, a, k[12], 20, -1926607734);

			a = hh(a, b, c, d, k[5], 4, -378558);
			d = hh(d, a, b, c, k[8], 11, -2022574463);
			c = hh(c, d, a, b, k[11], 16,  1839030562);
			b = hh(b, c, d, a, k[14], 23, -35309556);
			a = hh(a, b, c, d, k[1], 4, -1530992060);
			d = hh(d, a, b, c, k[4], 11,  1272893353);
			c = hh(c, d, a, b, k[7], 16, -155497632);
			b = hh(b, c, d, a, k[10], 23, -1094730640);
			a = hh(a, b, c, d, k[13], 4,  681279174);
			d = hh(d, a, b, c, k[0], 11, -358537222);
			c = hh(c, d, a, b, k[3], 16, -722521979);
			b = hh(b, c, d, a, k[6], 23,  76029189);
			a = hh(a, b, c, d, k[9], 4, -640364487);
			d = hh(d, a, b, c, k[12], 11, -421815835);
			c = hh(c, d, a, b, k[15], 16,  530742520);
			b = hh(b, c, d, a, k[2], 23, -995338651);

			a = ii(a, b, c, d, k[0], 6, -198630844);
			d = ii(d, a, b, c, k[7], 10,  1126891415);
			c = ii(c, d, a, b, k[14], 15, -1416354905);
			b = ii(b, c, d, a, k[5], 21, -57434055);
			a = ii(a, b, c, d, k[12], 6,  1700485571);
			d = ii(d, a, b, c, k[3], 10, -1894986606);
			c = ii(c, d, a, b, k[10], 15, -1051523);
			b = ii(b, c, d, a, k[1], 21, -2054922799);
			a = ii(a, b, c, d, k[8], 6,  1873313359);
			d = ii(d, a, b, c, k[15], 10, -30611744);
			c = ii(c, d, a, b, k[6], 15, -1560198380);
			b = ii(b, c, d, a, k[13], 21,  1309151649);
			a = ii(a, b, c, d, k[4], 6, -145523070);
			d = ii(d, a, b, c, k[11], 10, -1120210379);
			c = ii(c, d, a, b, k[2], 15,  718787259);
			b = ii(b, c, d, a, k[9], 21, -343485551);

			x[0] = add32(a, x[0]);
			x[1] = add32(b, x[1]);
			x[2] = add32(c, x[2]);
			x[3] = add32(d, x[3]);
		}

		function cmn(q, a, b, x, s, t) {
			a = add32(add32(a, q), add32(x, t));
			return add32((a << s) | (a >>> (32 - s)), b);
		}

		function ff(a, b, c, d, x, s, t) {
			return cmn((b & c) | ((~b) & d), a, b, x, s, t);
		}

		function gg(a, b, c, d, x, s, t) {
			return cmn((b & d) | (c & (~d)), a, b, x, s, t);
		}

		function hh(a, b, c, d, x, s, t) {
			return cmn(b ^ c ^ d, a, b, x, s, t);
		}

		function ii(a, b, c, d, x, s, t) {
			return cmn(c ^ (b | (~d)), a, b, x, s, t);
		}

		function md51(s) {
			var n = s.length,
			state = [1732584193, -271733879, -1732584194, 271733878], i;
			for (i=64; i<=s.length; i+=64) {
				md5cycle(state, md5blk(s.substring(i-64, i)));
			}
			s = s.substring(i-64);
			var tail = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
			for (i=0; i<s.length; i++) {
				tail[i>>2] |= s.charCodeAt(i) << ((i%4) << 3);
			}
			tail[i>>2] |= 0x80 << ((i%4) << 3);
			if (i > 55) {
				md5cycle(state, tail);
				for (i=0; i<16; i++) {
					tail[i] = 0;
				}
			}
			tail[14] = n*8;
			md5cycle(state, tail);
			return state;
		}

		function md5blk(s) { /* I figured global was faster.   */
			var md5blks = [], i; /* Andy King said do it this way. */
			for (i=0; i<64; i+=4) {
				md5blks[i>>2] = s.charCodeAt(i)
								+ (s.charCodeAt(i+1) << 8)
								+ (s.charCodeAt(i+2) << 16)
								+ (s.charCodeAt(i+3) << 24);
			}
			return md5blks;
		}

		var hex_chr = '0123456789abcdef'.split('');

		function rhex(n) {
			var s='', j=0;
			for(; j<4; j++) {
				s += hex_chr[(n >> (j * 8 + 4)) & 0x0F]
					+ hex_chr[(n >> (j * 8)) & 0x0F];
			}
			return s;
		}

		function hex(x) {
			for (var i=0; i<x.length; i++) {
				x[i] = rhex(x[i]);
			}
			return x.join('');
		}

		function add32(a, b) {
			return (a + b) & 0xFFFFFFFF;
		}

		function md5(s) {
			return hex(md51(s));
		}

		return {
			MD5: md5
		};
	})();

	var g_storage = (function () {
		var m_branch = Ss.prefs.getBranch("extensions.c7e3ccfd0398411b8607fa4ae25b4cd3.storage");
		var nsIPrefLocalizedString = Ci.nsIPrefLocalizedString;

		function getValue(name) {
			var stringConverter = m_branch.getComplexValue(name, nsIPrefLocalizedString);
			var res = null;
			try {
				res = JSON.parse(stringConverter.data);
			} catch (err) {}
			return res;
		}

		function setValue(name, value) {
			var stringConverter = Cc["@mozilla.org/pref-localizedstring;1"].createInstance(nsIPrefLocalizedString);
			stringConverter.data = JSON.stringify(value);
			m_branch.setComplexValue(name, nsIPrefLocalizedString, stringConverter);
		}

		function clearValue(name) {
			m_branch.clearUserPref(name);
		}

		function getKeys() {
			var array = [], count = { value: 0 };
			try {
				array = m_branch.getChildList("", count);
			} catch (error) {
			}
			return array;
		}
		
		var keys = getKeys();
		var pairs = {};
		for (var i = 0; i < keys.length; i++) {
			try {
				pairs[keys[i]] = getValue(keys[i]);
			} catch(error) {}
		}

		return {
			getValue : function (name) {
				return pairs[name];
			},
			setValue: function (name, value) {
				setValue(name, value);
				if (!pairs.hasOwnProperty(name)) {
					keys.push(name);
				}
				pairs[name] = value;
			},
			clear : function () {
				for (var i = 0; i < keys.length; i++) {
					clearValue(keys[i]);
				}
				keys = [];
				pairs = {};
			},
			getAll: function () {
				return pairs;
			}
		};
	})();

	var g_popupWindow = (function () {

		var xulWindow;
		var xulBrowser;
		var htmlWindow;
		var m_callback;

		function htmlWindowDOMContentLoaded(evt) {
			if (evt && evt.explicitOriginalTarget && evt.explicitOriginalTarget.defaultView) {
				xulBrowser.removeEventListener('DOMContentLoaded', htmlWindowDOMContentLoaded);
				htmlWindow = evt.explicitOriginalTarget.defaultView;
				htmlWindowLoaded();
			}
		}

		function close () {
			removeEventListeners();
			xulWindow.close();
			xulWindow = null;
		}

		function folderChoose (evt) {
			var _win = evt.target;
			var nsIFilePicker = Components.interfaces.nsIFilePicker;
			var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
			fp.init(_win, 'Create File', nsIFilePicker.modeGetFolder);
			if (fp.show() == nsIFilePicker.returnOK) {
				close();
				if (m_callback) {
					m_callback(fp.file.path);
				}
			}
		}

		function addEventListeners (_win) {
			_win.addEventListener("popup:folderChoose", folderChoose);
		}

		function removeEventListeners () {
			if (htmlWindow) {
				htmlWindow.addEventListener("popup:folderChoose", folderChoose);
			}
		}

		function htmlWindowLoaded () {
			addEventListeners(htmlWindow);
		}

		function xulWindowLoaded () {
			removeEventListeners();
			xulWindow.removeEventListener("load", xulWindowLoaded);
			xulBrowser = xulWindow.document.getElementById("browser");
			if (xulBrowser) {
				if (xulBrowser.contentWindow && xulBrowser.contentWindow.document && xulBrowser.contentWindow.document.body) {
					htmlWindow = xulBrowser.contentWindow;
					htmlWindowLoaded();
				} else {
					xulBrowser.addEventListener('DOMContentLoaded', htmlWindowDOMContentLoaded);
				}
			}
		}

		function openBindFolder (callback) {
			if (xulWindow) close();
			m_callback = callback;
			xulWindow = watcher.openWindow(null, "chrome://{c7e3ccfd-0398-411b-8607-fa4ae25b4cd3}/content/bindFolder/popup.xul","","width=200,height=200,titlebar=yes,scrollbar=no,dependent=0,close=yes,chrome,centerscreen", null);
			xulWindow.addEventListener("load", xulWindowLoaded);
		}

		return {
			openBindFolder: openBindFolder
		}
	})();

	var g_FILE = (function () {
		Components.utils.import("resource://gre/modules/FileUtils.jsm");
		Components.utils.import("resource://gre/modules/NetUtil.jsm");

		function readnsIFile(fileName, callback){
			var nsiFile = new FileUtils.File(fileName);
			if (!nsiFile.exists()) {
				return callback(null, 1, null);
			}
			NetUtil.asyncFetch(nsiFile, function (inputStream, status) {
				var data = '';
				try {
					data = NetUtil.readInputStreamToString(inputStream, inputStream.available());
				} catch (ex) {}
				callback(data, status, nsiFile);
			});
		}

		function writeToFile(fileName, content, callback) {
			var nsiFile = new FileUtils.File(fileName);
			writeToNsiFile(nsiFile, content, callback);
		}

		function writeToNsiFile(nsiFile, content, callback) {
			var ostream = FileUtils.openSafeFileOutputStream(nsiFile);
			var converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].
							createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
			converter.charset = "UTF-8";
			var istream = converter.convertToInputStream(content);

			NetUtil.asyncCopy(istream, ostream, function (status) {
				if (!Components.isSuccessCode(status)) {
					callback && callback(false);
				} else {
					callback && callback(true);	
				}
				delete nsiFile;
			});
		}
		function createFileInFolder (folderPath, fileName) {
			try {
				var file = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
				file.initWithPath(folderPath);
				if(!file.exists()) {
					return null;
				}
				// Append the file name.
				file.append(fileName);
				if (!file.exists()) {
					file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, FileUtils.PERMS_FILE);
				}
				// do whatever you need to the created file
				return file.path;
			} catch (ex) {
			}
		}		
		return {
			readFile: readnsIFile,
			writeToFile: writeToFile,
			writeToNsiFile: writeToNsiFile,
			createFileInFolder: createFileInFolder
		}
	})();

	var g_tasks = (function () {

		var m_tasks = {};
		var browserWindow = {};
		var fileContent = {};

		function fireEventS (taskName, typeEvent, contentEvent) {
			fireEvent(taskName, typeEvent, contentEvent);
		}
		function fireEvent (taskName, typeEvent, contentEvent) {
			var windows = m_tasks[taskName], _evt;
			for (var i = 0 , li = windows.length ; i < li ; ++i) {
				_evt = windows[i].document.createEvent('CustomEvent');
				_evt.initCustomEvent(typeEvent, true, true, JSON.stringify(contentEvent));
				windows[i].dispatchEvent(_evt);
			}
		}

		function boundFile (taskName, filePath, originalContent, isCreated) {
			if (isCreated === true) {
				var data = {filePath: filePath, md5: CryptoJS.MD5(originalContent).toString()};
				g_storage.setValue(taskName, data);
				g_FILE.writeToFile(filePath, originalContent, function (isOK) {
					if (false === isOK) {
						g_storage.setValue(taskName, {});
						fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});
					} else {
						fireEventS(taskName, 'plugin:fileBound', {filePath: filePath, content: originalContent});
					}
				});
			} else {
				g_FILE.readFile(filePath, function (content, status, nsiFile) {
					if (!nsiFile) {
						g_storage.setValue(taskName, {});
						fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});
					} else {
						if (!content) {
							var data = {filePath: filePath, md5: CryptoJS.MD5(originalContent).toString()};
							g_storage.setValue(taskName, data);
							fireEventS(taskName, 'plugin:fileBound', {filePath: filePath, content: originalContent});
							g_FILE.writeToNsiFile(nsiFile, originalContent, function (isOK) {
								if (false === isOK) {
									g_storage.setValue(taskName, {});
									fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});
								}
							});
						} else {
							delete nsiFile;
							var data = {filePath: filePath, md5: CryptoJS.MD5(content).toString()};
							g_storage.setValue(taskName, data);
							fireEventS(taskName, 'plugin:fileBound', {filePath: filePath, content: content});
						}	
					}
				});
			}
		}

		function webContentChanged (evt) {
			var taskName = getTaskFromHref(evt.target.location.href);
			var taskInfo = g_storage.getValue(taskName);
			if (taskInfo && taskInfo.filePath) {
				var filePath = taskInfo.filePath;
				var oldMD5 = taskInfo.md5;
				var content = evt.detail.content;
				var currMD5 = CryptoJS.MD5(content).toString();
				if (filePath && oldMD5 !== currMD5) {
					g_storage.setValue(taskName, {filePath: filePath, md5: CryptoJS.MD5(content).toString()});
					g_FILE.writeToFile(filePath, content, function (isOK) {
						if (false === isOK) {
							g_storage.setValue(taskName, {});
							fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});	
						}
					});
				}
			}
		}

		function webBindFolder (evt) {
			var taskName = getTaskFromHref(evt.target.location.href);
			var webContent = evt.detail.content;
			g_popupWindow.openBindFolder(function (folderPath) {
				g_storage.setValue('defaultFolder', {filePath: folderPath});
				var filePath = g_FILE.createFileInFolder(folderPath, taskName.split("_").join("."));
				doIt(taskName, filePath, webContent, evt, BOUND_FILE.defaultFolder);
			});
		}

		function webUnbindFolder (evt) {
			var taskName = getTaskFromHref(evt.target.location.href);
			fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});	

			// Clear tasks from local data.
			for (var taskName in m_tasks) {
				g_storage.setValue(taskName, {});
				delete m_tasks[taskName];
			}

			// Clear all tasks from storage.
			var tasks = g_storage.getAll();
			for (var taskName in tasks) {
				if ("logtime" !== taskName) {
					g_storage.setValue(taskName, {});
				}
			}
		}

		function getTaskFromHref (url) {
			var task = url.match(/checkio\.org(:.+)?\/mission\/(.+)\/solve/)
			if (task && task.length === 3 && task[2]) {
				var exts = url.match(/https?:\/\/(.+)\.checkio\.org/);
				var taskExt = (exts && exts.length === 2 ? exts[1]: "py");
				return task[2] + "_" + taskExt;
			}
			return null;
		}

		function pluginReady (taskName, filePath, content) {
			console.log("pluginReady");
			fireEventS(taskName, 'plugin:ready', {filePath: filePath, content: content, version: '0.0.0.14'});
		}

		function doIt (taskName, filePath, webContent, evt, type) {
			g_FILE.readFile(filePath, function (content, status, nsiFile) {
				if (!nsiFile) {
					g_storage.setValue(taskName, {});
					if (type === BOUND_FILE.file) {
						pluginReady(taskName, '', '');
					} else if (type === BOUND_FILE.defaultFolder) {
						webReady(evt);
					}
				} else {
					if (content) {
						var data = {filePath: filePath, md5: CryptoJS.MD5(content).toString()};
						g_storage.setValue(taskName, data);
						delete nsiFile;
						pluginReady(taskName, filePath, content);
					} else {
						var data = {filePath: filePath, md5: CryptoJS.MD5(webContent).toString()};
						g_storage.setValue(taskName, data);
						pluginReady(taskName, filePath, webContent);
						g_FILE.writeToNsiFile(nsiFile, webContent, function (isOK) {
							if (false === isOK) {
								g_storage.setValue(taskName, {});
								fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});
							}
						})
					}
				}
			});
		}

		function webReady (evt) {
			var taskName = getTaskFromHref(evt.target.location.href);
			var webContent = evt.detail.content;
			var taskInfo = g_storage.getValue(taskName);
			var defaultFolder = g_storage.getValue('defaultFolder');
			if (taskInfo && taskInfo.filePath) {
				var filePath = taskInfo.filePath;
				doIt(taskName, filePath, webContent, evt, BOUND_FILE.file);
			} else if (defaultFolder && defaultFolder.filePath) {
				var filePath = g_FILE.createFileInFolder(defaultFolder.filePath, taskName.split("_").join("."));
				if (filePath) {
					doIt(taskName, filePath, webContent, evt, BOUND_FILE.defaultFolder);
				} else {
					g_storage.setValue('defaultFolder', {});
					pluginReady(taskName, '', '');
				}
			} else {
				pluginReady(taskName, '', '');
			}
		}

		function webStartSync (evt) {
			var taskName = getTaskFromHref(evt.target.location.href);
			evt.target.addEventListener('focus', function () {
				var taskInfo = g_storage.getValue(taskName);
				if (taskInfo && taskInfo.filePath) {
					var filePath = taskInfo.filePath;
					g_FILE.readFile(filePath, function (content, status, nsiFile) {
						if (!nsiFile) {
							g_storage.setValue(taskName, {});
							fireEventS(taskName, 'plugin:fileBound', {filePath: '', content: ''});
						} else {
							delete nsiFile;
							var taskInfo = g_storage.getValue(taskName);
							var currMD5 = CryptoJS.MD5(content).toString();
							if (taskInfo && taskInfo.md5 && taskInfo.md5 !== currMD5) {
								g_storage.setValue(taskName, {filePath: filePath, md5: currMD5});
								fireEventS(taskName, 'plugin:contentChanged', {filePath: filePath, content: content});
							}
						}
					});
				} else {
					pluginReady(taskName, '', '');
				}
			});
		}

		function addEventListeners (_window) {
			_window.addEventListener("web:contentChanged", webContentChanged);
			_window.addEventListener("web:bindFolder", webBindFolder);
			_window.addEventListener("web:unbindFolder", webUnbindFolder);
			_window.addEventListener("web:ready", webReady);
			_window.addEventListener("web:startSync", webStartSync);
		}

		function removeEventListeners (_window) {
			try {
				_window.removeEventListener("web:contentChanged", webContentChanged);
				_window.removeEventListener("web:bindFolder", webBindFolder);
				_window.removeEventListener("web:unbindFolder", webUnbindFolder);
				_window.removeEventListener("web:ready", webReady);
				_window.removeEventListener("web:startSync", webStartSync);
			} catch (ex) {
			}
		}

		function init (task, win) {
			if (m_tasks[task]) {
				for (var i = 0 , li = m_tasks[task].length ; i < li ; ++i) {
					removeEventListeners(m_tasks[task][i]);
				}
			}
			m_tasks[task] = [win];		
			addEventListeners(win);
		}
		function clear (task, win) {
			if (m_tasks[task]) {
				removeEventListeners(win);
			}
		}

		return {
			init: init,
			clear: clear,
			boundFile: boundFile
		}
	})();

	var g_tabHandler = (function () {

		var m_browsers = [];

		function DOMContentLoaded (event) {
			var win = event.explicitOriginalTarget.defaultView;
			console.log("DOMContentLoaded");
			if (win && win.top) {
				var url = '' + win.location.href;
				var task = url.match(/checkio\.org(:.+)?\/mission\/(.+)\/solve/)
				if (task && task.length === 3 && task[2]) {
					var exts = url.match(/https?:\/\/(.+)\.checkio\.org/);
					var taskExt = (exts && exts.length === 2 ? exts[1]: "py");
					g_tasks.init(task[2]+"_"+taskExt, win);
				}
			}
		}

		function tabOpened (browser) {
			var _id = browser.getAttribute('__id');
			if (!_id) {
				browser.setAttribute('__id', Math.random());
				m_browsers.push(browser);
				browser.addEventListener("DOMContentLoaded", DOMContentLoaded);
			}
		}

		function tabClosed (browser) {
			browser.removeEventListener("DOMContentLoaded", DOMContentLoaded);
			var index = m_browsers.indexOf(browser);
			if (-1 !== index) {
				m_browsers.splice(index, 1);
			}
		}

		function initLoadedTabs (gBrowser) {
			var length = gBrowser.browsers.length;
			if (length) {
				for (var i = 0; i < length; ++i) {
					tabOpened(gBrowser.getBrowserAtIndex(i));
				}
			}
		}

		function windowLoaded (win) {
			var gBrowser = win.gBrowser;
			console.log("windowLoaded");
			var _id = gBrowser.getAttribute('__id');
			if (!_id) {
				gBrowser.setAttribute('__id', Math.random());
				var tabContainer = gBrowser.tabContainer;
				tabContainer.addEventListener("TabOpen", function (event) {
					tabOpened(gBrowser.getBrowserForTab(event.target));
				});
				tabContainer.addEventListener("TabClose", function (event) {
					tabClosed(gBrowser.getBrowserForTab(event.target));
				});
			} else {
			}
			initLoadedTabs(gBrowser);
		}
		function clear () {
			for (var i = m_browsers.length - 1 ; i >= 0 ; --i) {
				tabClosed(m_browsers[i]);
			}
		}
		
		return {
			windowLoaded: windowLoaded,
			clear: clear
		}
	})();

	var g_windowHandler = (function () {

		var isInited = false;
		var m_windows = [];

		function isBrowserWindow(win) {
			if (win && win.document && win.document.documentElement) {
				return "navigator:browser" === win.document.documentElement.getAttribute("windowtype");
			}
			return false;
		}
		
		function attachOnloadHandler (win) {
			m_windows.push(win);
			win.addEventListener("load", browserWindowLoadEventHandler, false);
		}
		
		function dettachOnloadHandler(win) {
			var index = m_windows.indexOf(win);
			if (-1 !== index) {
				m_windows.splice(index, 1);
			}
			win.removeEventListener("load", browserWindowLoadEventHandler, false);
		}
		
		function browserWindowLoadEventHandler() {
			dettachOnloadHandler(this);
			onLoaded(this);
		}

		function onLoaded (win) {
			if (isBrowserWindow(win)) {
				g_tabHandler.windowLoaded(win);
			}
		}

		function onUnloaded () {
			if (isBrowserWindow(window)) {
				onUnloaded(window);
			}
		}

		function notificationHandler (win, reason) {
			if ("domwindowopened" === reason) {
				attachOnloadHandler(win);
			} else if ("domwindowopened" === reason) {
				onUnloaded(win);
			}
		}

		function getEnumerators () {
			var brWin;
			var brsEnum = mediator.getEnumerator("navigator:browser");		
			while (brsEnum.hasMoreElements()) {
				brWin = brsEnum.getNext();
				if ("complete" === brWin.document.readyState) {
					onLoaded(brWin);
				} else {
					attachOnloadHandler(brWin);
				}
			}
		}

		var XMLHttpRequest = Components.Constructor("@mozilla.org/xmlextras/xmlhttprequest;1", "nsIXMLHttpRequest");

		function sendlog () {
			var timerId = null;
			var oneDay = 24 * 60 * 60 * 1000;
			var partDay = 3 * 60 * 60 * 1000;
			function sendRequest (currentTime) {
				var xhr = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
				xhr.open("GET", "https://checkio.org/log/web-plugin/info/?version=0.0.0.14&browser=firefox", true);
				xhr.onload = function (e) {
					if (xhr.readyState === 4) {
						if (xhr.status === 200) {
							g_storage.setValue('logtime', currentTime);
						}
					}
				};
				xhr.onerror = function (e) {
				};
				xhr.send(null);	
			}

			(function check () {
				timers.clearTimeout(timerId);
				var currentTime = Date.now();
				var cacheTime = parseInt(g_storage.getValue('logtime'));
				if (!cacheTime || (currentTime - cacheTime - oneDay >= 0)) {
					sendRequest(currentTime);
				}
				timerId = timers.setTimeout(check, partDay);
			})();
		}

		function start () {
			getEnumerators();
			watcher.registerNotification(notificationHandler);
			isInited = true;
			sendlog();
		}

		function clear () {
			if (true === isInited) {
				for (var i = m_windows.length -1 ; i >= 0; --i) {
					dettachOnloadHandler(m_windows[i], true);
				}
				watcher.unregisterNotification(notificationHandler);
				var brWin, gBrowser;
				var brsEnum = mediator.getEnumerator("navigator:browser");		
				while (brsEnum.hasMoreElements()) {
					brWin = brsEnum.getNext();
					gBrowser = brWin.gBrowser;
					if (gBrowser) {
						gBrowser.removeAttribute('__id');
					}
				}
			}
		}
		
		return {
			start: start,
			clear: clear
		};
	})();
	return {
		install: function (data, reason) {
			console.log("install");
			g_windowHandler.start();
		},
		shutdown: function (data, reason) {
			console.log("shutdown");
			if (reason == APP_SHUTDOWN) {
				return;
			}
			g_windowHandler.clear();
			g_tabHandler.clear();	
		},
		uninstall: function (data, reason) {
			console.log("uninstall");
			if (reason === ADDON_UNINSTALL) {
				g_storage.clear();
			}
		}
	}
})();

function startup(data, reason) {
	g_extension.install(data, reason);
}

function shutdown(data, reason) {
	g_extension.shutdown(data, reason);
}

function install(data, reason) {
	
}

function uninstall(data, reason) {
	g_extension.uninstall(data, reason);
}