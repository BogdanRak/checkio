(function (_win, _doc) {
	var body;
	(function init () {
		if ((body = _doc.body)) {
			ready();
		} else {
			setTimeout(function () {
				init();
			}, 10);
		}
	})();

	function log (str) {
		
	}

	function fireEvent (type, data) {
		var _evt = _doc.createEvent('CustomEvent');
		_evt.initCustomEvent(type, true, true, data);
		_win.dispatchEvent(_evt);
	}
	function chooseFolder (evt) {
		fireEvent('popup:folderChoose', {});
	}
	function ready () {
		var choose = _doc.getElementById('choose'); 
		if (choose) {
			choose.addEventListener('click', chooseFolder);
		}
	}
})(window, window.document)