(function (_win, _doc) {
	function fireEvent (typeEvent, contentEvent) {
		var _evt = _doc.createEvent('CustomEvent');
		_evt.initCustomEvent(typeEvent, true, true, contentEvent);
		_win.dispatchEvent(_evt);
	}
	_win.addEventListener('plugin:fileBoundS', function (evt) {
		var message = evt.detail;
		fireEvent('plugin:fileBound', JSON.parse(message));
	});
	_win.addEventListener('plugin:contentChangedS', function (evt) {
		var message = evt.detail;
		fireEvent('plugin:contentChanged', JSON.parse(message));
	});
	_win.addEventListener('plugin:readyS', function (evt) {
		var message = evt.detail;
		fireEvent('plugin:ready', JSON.parse(message));
	});
	_win.addEventListener('plugin:ready', function (evt) {
		var message = evt.detail;
		console.log(message);
	});
	_win.addEventListener("web:contentChanged", function (evt) {
		console.log(evt.detail.content.replace(/(\n|\r)/g, ''));
	});
	// _win.addEventListener("web:bindFile", webBindFile);
	// _win.addEventListener("web:unbindFile", webUnbindFile);
	// _win.addEventListener("web:ready", webReady);
	// _win.addEventListener("web:startSync", webStartSync);
})(window, document);