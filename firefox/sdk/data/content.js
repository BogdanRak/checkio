"use strict"
const TASK_REGEX = /checkio\.org(:.+)?\/mission\/(.+)\/solve/;
const TASK_TYPE_REGEX = /https?:\/\/(.+)\.checkio\.org/;
const WEBEVENTS = {
  contentChanged:   "web:contentChanged",
  unbindFolder:     "web:unbindFolder",
  bindFolder:       "web:bindFolder",
  startSync:        "web:startSync",
  ready:            "web:ready"
};
const PLUGINEVENTS = {
  contentChanged:   "plugin:contentChanged",
  fileBound:        "plugin:fileBound",
  ready:            "plugin:ready",
};
const CONTENTEVENTS = {
  contentChanged:   "content:contentChanged",
  unbindFolder:     "content:unbindFolder",
  bindFolder:       "content:bindFolder",
  webReady:         "content:webReady",
  focus:            "content:focus"
};

function webContentChanged(evt){
  evt.detail.taskName = this.taskName;
  self.port.emit(CONTENTEVENTS.contentChanged, evt.detail);
};

function webBindFolder(evt){
  evt.detail.taskName = this.taskName;
  self.port.emit(CONTENTEVENTS.bindFolder, evt.detail);
};

function webUnbindFolder(evt){
  self.port.emit(CONTENTEVENTS.unbindFolder, { taskName: this.taskName });
};

function webReady(evt){
  evt.detail.taskName = this.taskName;
  self.port.emit(CONTENTEVENTS.webReady, evt.detail);
};

function winFocused(){
  self.port.emit(CONTENTEVENTS.focus, { taskName: this.taskName });
};

function webStartSync(evt){
  window.addEventListener('focus', winFocused.bind({ taskName: this.taskName }));
  webStartSync = function (){};
};

/**
 *  Add event listeners for Custom Web Events.
 *
 *  @param {string} taskName Task name.
 */
const initWebListeners = (taskName)=>{
  window.addEventListener(WEBEVENTS.contentChanged, webContentChanged.bind({taskName}));
  window.addEventListener(WEBEVENTS.bindFolder,     webBindFolder.bind({taskName}));
  window.addEventListener(WEBEVENTS.unbindFolder,   webUnbindFolder.bind({taskName}));
  window.addEventListener(WEBEVENTS.ready,          webReady.bind({taskName}));
  window.addEventListener(WEBEVENTS.startSync,      webStartSync.bind({taskName}));
};

/**
 *  Fire event to Web Page
 *
 *  @param {object} contentEvent Event from background process.
 */
function fireWebEvent(contentEvent){
  console.log("fireWebEvent", contentEvent.taskName, this.taskName, contentEvent.taskName === this.taskName);
  if (contentEvent.taskName === this.taskName){
    let eventName = this.eventName;
    let evt = window.document.createEvent('CustomEvent');
    evt.initCustomEvent(eventName, true, true, JSON.stringify(contentEvent));
    window.dispatchEvent(evt);
  }
}

/**
 *  Add event listeners from background.
 *
 *  @param {string} taskName Task name.
 */
const initAddonListeners = (taskName)=>{
  self.port.on(PLUGINEVENTS.fileBound,      fireWebEvent.bind({eventName: PLUGINEVENTS.fileBound,       taskName}));
  self.port.on(PLUGINEVENTS.ready,          fireWebEvent.bind({eventName: PLUGINEVENTS.ready,           taskName}));
  self.port.on(PLUGINEVENTS.contentChanged, fireWebEvent.bind({eventName: PLUGINEVENTS.contentChanged,  taskName}));
};

/**
 *  Get task name from Location href.
 *  @return {string|null} task name.
 */
const getTaskName = ()=>{
  let taskType = location.href.match(TASK_REGEX);
  if (taskType && taskType[2]) {
    let exts = location.href.match(TASK_TYPE_REGEX);
    let taskName = taskType[2];
    let taskExt = (exts && exts.length === 2 ? exts[1]: "py");
    taskName = taskName + "_" + taskExt;
    return taskName;
  }
  return null;
};

/**
 *  Main function.
 *
 *  Got taskName from location href.
 *  Register event listeners for web events and background events.
 */
(function main(){
  let taskName = getTaskName();
  if (taskName){
    initWebListeners(taskName);
    initAddonListeners(taskName);
  }
})();
