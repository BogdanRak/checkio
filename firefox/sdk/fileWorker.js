let { Cc, Ci, Cu } = require('chrome');
let { FileUtils } = Cu.import("resource://gre/modules/FileUtils.jsm");
let { NetUtils } = Cu.import("resource://gre/modules/NetUtil.jsm");

const nsIFilePicker = Ci.nsIFilePicker;
const BOUND_FILE = { 'file': 1, 'defaultFolder': 3 };

/**
 *  Choose Folder.
 *  Open form to Choose Folder.
 */
const chooseFolder = ()=>{
  return new Promise((succes, fail)=>{
    if (window === null || typeof window !== "object") {
      var window = require('sdk/window/utils').getMostRecentBrowserWindow();
    }
    let filePicker = Cc["@mozilla.org/filepicker;1"]
                   .createInstance(nsIFilePicker);
    filePicker.init(window, "Choose Folder", nsIFilePicker.modeGetFolder);
    filePicker.appendFilters(nsIFilePicker.filterAll);
    let pickerStatus = filePicker.show();
    if (pickerStatus == nsIFilePicker.returnOK || pickerStatus == nsIFilePicker.returnReplace) {
      let file = filePicker.file;
      // Get the path as string. Note that you usually won't 
      // need to work with the string paths.
      let path = filePicker.file.path;
      succes(path);
    }
  });
}

/**
 *  Read from file by file path
 *
 *  @param {string} fileName File Path
 *
 *  @return {Promise<string>} Promise with file content.
 */
const readnsIFile = (fileName)=>{
  return new Promise((succes, fail)=>{
    let nsiFile = new FileUtils.File(fileName);
    if (!nsiFile.exists()) {
      return succes({content: null, status: 1, nsiFile: null});
    }
    NetUtil.asyncFetch(nsiFile, (inputStream, status)=>{
      let content = '';
      try {
        content = NetUtil.readInputStreamToString(inputStream, inputStream.available());
      } catch (ex) {}
      succes({content, status, nsiFile});
    });
  });
};

/**
 *  Write content to NSI file
 *
 *  @param {nsiFile} nsiFile File Object.
 *  @param {string} content New content for file.
 *
 *  @return {Promise<boolean>}
 */
const writeToNsiFile = (nsiFile, content)=>{
  return new Promise((succes, fail)=>{
    let ostream = FileUtils.openSafeFileOutputStream(nsiFile);
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"]
                    .createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = "UTF-8";
    let istream = converter.convertToInputStream(content);
    NetUtil.asyncCopy(istream, ostream, (status)=>{
      if (!Components.isSuccessCode(status)) {
        succes(false);
      } else {
        succes(true); 
      }
      delete nsiFile;
    });
  });
};

/**
 *  Write content to file
 *
 *  @param {string} fileName File Path.
 *  @param {string} content New content for file.
 */
const writeToFile = (fileName, content)=>{
  let nsiFile = new FileUtils.File(fileName);
  return writeToNsiFile(nsiFile, content);
};

/**
 *  Create file in folder.
 *
 *  @param {string} folderPath File Path.
 *  @param {string} fileName File Name.
 */
const createFileInFolder = (folderPath, fileName)=>{
  try {
    let file = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
    file.initWithPath(folderPath);
    if(!file.exists()) {
      return null;
    }
    // Append the file name.
    file.append(fileName);
    if (!file.exists()) {
      file.createUnique(Ci.nsIFile.NORMAL_FILE_TYPE, FileUtils.PERMS_FILE);
    }
    // do whatever you need to the created file
    return file.path;
  } catch (ex) {
  }
}

exports.chooseFolder = chooseFolder;
exports.readFile = readnsIFile;
exports.writeToNsiFile = writeToNsiFile;
exports.writeToFile = writeToFile;
exports.createFileInFolder = createFileInFolder;
exports.BOUND_FILE = BOUND_FILE;

