"use strict";

let self = require("sdk/self");
let g_pageMod = require("sdk/page-mod");
let g_storage = require("sdk/simple-storage").storage;
let FileWorker = require("./fileWorker");
let CryptoJS = require("./utils/crypto");

let { Cc, Ci } = require('chrome');
let workers = [];

const VERSION = "1.0.2.10";
const BOUND_FILE = FileWorker.BOUND_FILE;
const BGEVENTS = {
  READY:            "plugin:ready",
  FILE_BOUND:       "plugin:fileBound",
  CONTENT_CHANGED:  "plugin:contentChanged"
};
const CONTENTEVENTS = {
  contentChanged:   "content:contentChanged",
  bindFolder:       "content:bindFolder",
  unbindFolder:     "content:unbindFolder",
  webReady:         "content:webReady",
  focus:            "content:focus"
};

/**
 *  Fire Event to content script.
 *
 *  @param {string} typeEvent Event Name.
 *  @param {string} contentEvent Content Event.
 *
 */
const fireEvent = (typeEvent, contentEvent)=>{
  let _workers = workers;
  for (let ctWorker of _workers){
    ctWorker.port.emit(typeEvent, contentEvent);
  }
};

/**
 *  Fire Event to content script.
 *
 *  @param {string} typeEvent Event Name.
 *  @param {string} contentEvent Content Event.
 *
 */
const pluginReady = (taskName, filePath, content)=>{
  fireEvent(BGEVENTS.READY, {filePath, content, version: VERSION, taskName });
};

/**
 *  Detach worker
 *
 *  @param {object} worker.
 *  @param {object[]} workerArray.
 *
 */
const detachWorker = (worker, workerArray)=>{
  let index = workerArray.indexOf(worker);
  if (index != -1){
    workerArray.splice(index, 1);
  }
};

/**
 *  @param {string} taskName
 *  @param {string} filePath
 *  @param {string} webContent
 *  @param {object} evt
 *  @param {BOUND_FILE} type
 *
 *  If we can't read file by filename - trying create file in default folder or send info that we do not have any info.
 *  If we CAN read file and have in file any content - send content to the content script.
 *  If we CAN read file and have not in file any content - add content to the file.
 */
function doIt(taskName, filePath, webContent, evt, type){
  FileWorker.readFile(filePath).then((data)=>{
    let content = data.content;
    let status = data.status;
    let nsiFile = data.nsiFile;
    if (!nsiFile) {
      g_storage[taskName] = {};
      if (type === BOUND_FILE.file) {
        pluginReady(taskName, '', '');
      } else if (type === BOUND_FILE.defaultFolder) {
        ctWebReady(evt);
      }
    } else {
      if (content) {
        let data = {filePath: filePath, md5: CryptoJS.MD5(content).toString()};
        g_storage[taskName] = data;
        nsiFile = null;
        pluginReady(taskName, filePath, content);
      } else {
        var data = {filePath: filePath, md5: CryptoJS.MD5(webContent).toString()};
        g_storage[taskName] = data;
        pluginReady(taskName, filePath, webContent);
        FileWorker.writeToNsiFile(nsiFile, webContent, function (isOK) {
          if (false === isOK) {
            g_storage[taskName] = {};
            fireEvent(BGEVENTS.FILE_BOUND, {filePath: '', content: '', taskName});
          }
        })
      }
    }
  });
}

/**
 *  WebReady Event Handler.
 * 
 *  @param {object} evt Event from Content script.
 *  @param {string} evt.taskName Task name.
 *  @param {string} evt.content Task content.
 *
 *  If we already have bound file to the task - 
 *  Add content to the file or send info to content script
 */
function ctWebReady(evt){
  let taskName = evt.taskName;
  let taskInfo = g_storage[taskName];
  let defaultFolder = g_storage["defaultFolder"];
  if (taskInfo && taskInfo.filePath) {
    let filePath = taskInfo.filePath;
    doIt(taskName, filePath, evt.content, evt, BOUND_FILE.file);
  } else if (defaultFolder && defaultFolder.filePath) {
    let filePath = FileWorker.createFileInFolder(defaultFolder.filePath, taskName.split("_").join("."));
    if (filePath) {
      doIt(taskName, filePath, evt.content, evt, BOUND_FILE.defaultFolder);
    } else {
      g_storage["defaultFolder"] = {};
      pluginReady(taskName, '', '');
    }
  } else {
    pluginReady(taskName, '', '');
  }
};

/**
 *  Focus Event listener.
 * 
 *  @param {object} evt Event from Content script.
 *  @param {string} evt.taskName Task name.
 *
 *  Check any changes in bound file.
 *  Read file and check md5 hash with previous content file version.
 */
function ctFocused(evt){
  let taskName = evt.taskName;
  let taskInfo = g_storage[taskName];
  if (taskInfo && taskInfo.filePath){
    let filePath = taskInfo.filePath;
    FileWorker.readFile(filePath).then((data)=>{
      let content = data.content;
      let status = data.status;
      let nsiFile = data.nsiFile;
      if (!nsiFile) {
        g_storage[taskName] = {};
        fireEvent(BGEVENTS.FILE_BOUND, {filePath: '', content: '', taskName});
      } else {
        nsiFile = null;
        let currMD5 = CryptoJS.MD5(content).toString();
        if (taskInfo && taskInfo.md5 && taskInfo.md5 !== currMD5) {
          g_storage[taskName] = {filePath, md5: currMD5};
          fireEvent(BGEVENTS.CONTENT_CHANGED, {filePath, content, taskName});
        }
      }
    }).catch(err=>{
      console.log("ctFocused", err);
    });
  }
}

/**
 *  Bind Folder Event Handler.
 * 
 *  @param {object} evt Event from Content script.
 *  @param {string} evt.taskName Task name.
 *  @param {string} evt.content Task content.
 *
 *  Choose folder then create a file and then add content to the file.
 */
function ctBindFolder(evt){
  let taskName = evt.taskName;
  let webContent = evt.content;
  FileWorker.chooseFolder().then((folderPath)=>{
    g_storage["defaultFolder"] = {filePath: folderPath};
    let filePath = FileWorker.createFileInFolder(folderPath, taskName.split("_").join("."));
    doIt(taskName, filePath, webContent, evt, BOUND_FILE.defaultFolder);
  }).catch(err=>{
    console.log(err);
  });
}

/**
 *  Conent Changed Event Hanlder.
 * 
 *  @param {object} evt Event from Content script.
 *  @param {string} evt.taskName Task name.
 *
 *  Write to bound file for the task new content if necessary. 
 */
function ctContentChanged(evt){
  let taskName = evt.taskName;
  let taskInfo = g_storage[taskName];
  if (taskInfo && taskInfo.filePath) {
    let filePath = taskInfo.filePath;
    let oldMD5 = taskInfo.md5;
    let content = evt.content;
    let currMD5 = CryptoJS.MD5(content).toString();
    if (filePath && oldMD5 !== currMD5) {
      g_storage[taskName] = {filePath, md5: CryptoJS.MD5(content).toString()};
      FileWorker.writeToFile(filePath, content).then((isOK)=>{
        if (false === isOK) {
          g_storage[taskName, {}];
          fireEvent(BGEVENTS.FILE_BOUND, {filePath: '', content: '', taskName});
        }
      }).catch(err=>{
        console.log(err);
      });
    }
  }
}

/**
 *  Unbind Folder Event Handler.
 *  
 *  @param {object} evt Event from Content script.
 *  @param {string} evt.taskName Task name.
 *
 *  Clearing storage and Send about that to Content.
 */
function ctUnbindFolder (evt) {
  let taskName = evt.taskName;
  g_storage[taskName] = {};
  fireEvent(BGEVENTS.FILE_BOUND, {filePath: '', content: '', taskName});
}

g_pageMod.PageMod({
  include: "*.checkio.org",
  contentScriptFile: "./content.js",
  contentScriptWhen: "start",
  attachTo: ["existing", "top"],
  onAttach: function(worker) {
    workers.push(worker);
    worker.port.on(CONTENTEVENTS.webReady, ctWebReady);
    worker.port.on(CONTENTEVENTS.focus, ctFocused);
    worker.port.on(CONTENTEVENTS.bindFolder, ctBindFolder);
    worker.port.on(CONTENTEVENTS.contentChanged, ctContentChanged);
    worker.port.on(CONTENTEVENTS.unbindFolder, ctUnbindFolder);
    worker.on('detach', function () {
      detachWorker(this, workers);
    });
  }
});
